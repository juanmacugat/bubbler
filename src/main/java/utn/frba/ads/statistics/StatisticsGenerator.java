package utn.frba.ads.statistics;

public class StatisticsGenerator {

	private BarChart barchart;
	private LineChart linechart;
	private PieChart piechart;

	public BarChart getBarchart() {
		return barchart;
	}

	public void setBarchart(BarChart barchart) {
		this.barchart = barchart;
	}

	public LineChart getLinechart() {
		return linechart;
	}

	public void setLinechart(LineChart linechart) {
		this.linechart = linechart;
	}

	public PieChart getPiechart() {
		return piechart;
	}

	public void setPiechart(PieChart piechart) {
		this.piechart = piechart;
	}

}
