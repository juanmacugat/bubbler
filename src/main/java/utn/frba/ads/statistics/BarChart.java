package utn.frba.ads.statistics;

import java.util.ArrayList;
import java.util.List;

public class BarChart {

	List<String> labels = new ArrayList<String>();
	List<Data> datasets = new ArrayList<Data>();

	public List<Data> getDatasets() {
		return datasets;
	}

	public void setDatasets(List<Data> datasets) {
		this.datasets = datasets;
	}

	public void addData(Data data) {
		this.datasets.add(data);
	}

	public void removeData(Data data) {
		this.datasets.remove(data);
	}

}
