package utn.frba.ads.utils;

import spark.Request;
import utn.frba.ads.entities.Usuario;

public class AuthenticationUtil {

	private static final String USER_SESSION_ID = "user";

	public static Usuario getAuthenticatedUser(Request request) {
		return request.session().attribute(USER_SESSION_ID);
	}

	public static void addAuthenticatedUser(Request request, Usuario user) {
		request.session().attribute(USER_SESSION_ID, user);
	}

	public static void removeAuthenticationUser(Request request) {
		request.session().removeAttribute(USER_SESSION_ID);
	}

}
