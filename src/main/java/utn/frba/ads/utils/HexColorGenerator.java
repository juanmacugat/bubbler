package utn.frba.ads.utils;

import java.awt.Color;
import java.util.Random;

public class HexColorGenerator {

	public static String generateHexColor() {

		Random rand = new Random();
		float r = rand.nextFloat();
		float g = rand.nextFloat();
		float b = rand.nextFloat();

		Color color = new Color(r, g, b);

		return "#" + Integer.toHexString(color.getRGB()).substring(2).toUpperCase();
	}

}
