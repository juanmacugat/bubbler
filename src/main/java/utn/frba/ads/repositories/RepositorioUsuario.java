package utn.frba.ads.repositories;

import java.util.List;

import org.uqbarproject.jpa.java8.extras.WithGlobalEntityManager;
import org.uqbarproject.jpa.java8.extras.transaction.TransactionalOps;

import utn.frba.ads.entities.Usuario;

public class RepositorioUsuario implements WithGlobalEntityManager, TransactionalOps {

	private static RepositorioUsuario instance = new RepositorioUsuario();

	public static RepositorioUsuario getInstance() {
		if (instance == null)
			return new RepositorioUsuario();

		return instance;
	}

	public List<Usuario> getUsuarios() {
		return entityManager().createQuery("from Usuario").getResultList();
	}

	public void agregarUsuario(Usuario usuario) {
		withTransaction(() -> {
			entityManager().persist(usuario);
		});
	}

	public Usuario searchByMail(String email) {
		return entityManager().createQuery("from Usuario u where u.username = :email", Usuario.class)
				.setParameter("email", email).getResultList().get(0);
	}

}
