package utn.frba.ads.controller;

import static spark.Spark.halt;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import utn.frba.ads.utils.PasswordUtil;

public class SignupController {

	public ModelAndView show(Request request, Response response) {
		return new ModelAndView(null, "register.hbs");
	}

	public Void register(Request request, Response response) {

		String nombre = request.queryParams("nombre");
		String email = request.queryParams("email");
		String password = request.queryParams("password");

		String hashpwd = PasswordUtil.hashPassword(password);

		// Chequear que no exista el usuario con dicho mail

		// User usuario = new User(nombre, email, hashpwd);
		// TeacherRepository.getInstance().addTeacher(usuario);
		// AuthenticationUtil.addAuthenticatedUser(request, usuario);

		response.redirect("/");
		halt();

		return null;
	}

}
