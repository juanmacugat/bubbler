package utn.frba.ads.controller;

import static spark.Spark.halt;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import utn.frba.ads.entities.Usuario;
import utn.frba.ads.repositories.RepositorioUsuario;
import utn.frba.ads.utils.AuthenticationUtil;
import utn.frba.ads.utils.PasswordUtil;

public class SigninController {

	public ModelAndView login(Request request, Response response) {

		String email = request.queryParams("email");
		String password = request.queryParams("password");
		Map<String, Object> map = new HashMap<>();

		if (Objects.isNull(email) || !email.isEmpty()) {

			Usuario usuario = RepositorioUsuario.getInstance().searchByMail(email);
			if (Objects.isNull(usuario)) {
				map.put("error", "El usuario no existe, debe registrarse en el sistema");
				return new ModelAndView(map, "enter.hbs");
			}

			if (PasswordUtil.verifyPassword(password, usuario.getPassword())) {
				AuthenticationUtil.addAuthenticatedUser(request, usuario);
				response.redirect("/");
				halt();
			} else {
				map.put("error", "La contraseña es incorrecta. Ingrese nuevamente");
				return new ModelAndView(map, "enter.hbs");
			}
		}
		map.put("error", "Debe ingresar nombre de usuario y contraseña");
		return new ModelAndView(map, "enter.hbs");
	}

	public ModelAndView show(Request request, Response response) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("title", "CManager - UTN FRBA");
		return new ModelAndView(map, "enter.hbs");
	}
}
