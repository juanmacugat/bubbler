package utn.frba.ads.controller;

import java.util.HashMap;
import java.util.Map;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import utn.frba.ads.entities.Usuario;
import utn.frba.ads.utils.AuthenticationUtil;

public class DashboardController {

	public ModelAndView show(Request request, Response response) {

		Map<String, Object> map = new HashMap<String, Object>();
		Usuario user = AuthenticationUtil.getAuthenticatedUser(request);
		map.put("user", user);

		// Aca tengo que calcular las estadisticas para mostrar

		return new ModelAndView(map, "dashboard.hbs");
	}

}
