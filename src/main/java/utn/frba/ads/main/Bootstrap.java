package utn.frba.ads.main;

import java.util.Arrays;
import java.util.List;

import org.uqbarproject.jpa.java8.extras.WithGlobalEntityManager;
import org.uqbarproject.jpa.java8.extras.transaction.TransactionalOps;

import utn.frba.ads.entities.Usuario;
import utn.frba.ads.repositories.RepositorioUsuario;
import utn.frba.ads.utils.PasswordUtil;

public class Bootstrap implements WithGlobalEntityManager, TransactionalOps {

	public static void main(String[] args) throws Exception {

		Usuario jmc = new Usuario("Juan Manuel Cugat", "juanma.cugat@gmail.com",
				PasswordUtil.hashPassword("prueba123"));

		new Bootstrap().run(Arrays.asList(jmc));
	}

	public void run(List<Usuario> usuarios) {

		usuarios.stream().forEach(usuario -> agregarUsuario(usuario));

		// agregarDocentes();
		// agregarGrupos();
		// agregarPlanillaAsistencias();
		// agregarAlumnos();
	}

	private void agregarUsuario(Usuario usuario) {
		withTransaction(() -> {
			RepositorioUsuario.getInstance().agregarUsuario(usuario);
		});
	}

	// private void agregarPlanillaAsistencias() {
	// RepositorioAsistencia.getInstance().agregarAsistencia(
	// new AsistenciaBuilder().setFecha(Date.valueOf(LocalDate.of(ANIO,
	// Month.MARCH, 16))).build());
	// RepositorioAsistencia.getInstance().agregarAsistencia(
	// new AsistenciaBuilder().setFecha(Date.valueOf(LocalDate.of(ANIO,
	// Month.MARCH, 23))).build());
	// RepositorioAsistencia.getInstance().agregarAsistencia(
	// new AsistenciaBuilder().setFecha(Date.valueOf(LocalDate.of(ANIO,
	// Month.MARCH, 30))).build());
	// RepositorioAsistencia.getInstance().agregarAsistencia(
	// new AsistenciaBuilder().setFecha(Date.valueOf(LocalDate.of(ANIO,
	// Month.APRIL, 6))).build());
	// RepositorioAsistencia.getInstance().agregarAsistencia(
	// new AsistenciaBuilder().setFecha(Date.valueOf(LocalDate.of(ANIO,
	// Month.APRIL, 13))).build());
	// RepositorioAsistencia.getInstance().agregarAsistencia(
	// new AsistenciaBuilder().setFecha(Date.valueOf(LocalDate.of(ANIO,
	// Month.MAY, 20))).build());
	// RepositorioAsistencia.getInstance().agregarAsistencia(
	// new AsistenciaBuilder().setFecha(Date.valueOf(LocalDate.of(ANIO,
	// Month.MAY, 27))).build());
	// RepositorioAsistencia.getInstance().agregarAsistencia(
	// new AsistenciaBuilder().setFecha(Date.valueOf(LocalDate.of(ANIO,
	// Month.MAY, 4))).build());
	// RepositorioAsistencia.getInstance().agregarAsistencia(
	// new AsistenciaBuilder().setFecha(Date.valueOf(LocalDate.of(ANIO,
	// Month.MAY, 11))).build());
	// RepositorioAsistencia.getInstance().agregarAsistencia(
	// new AsistenciaBuilder().setFecha(Date.valueOf(LocalDate.of(ANIO,
	// Month.MAY, 18))).build());
	// RepositorioAsistencia.getInstance().agregarAsistencia(
	// new AsistenciaBuilder().setFecha(Date.valueOf(LocalDate.of(ANIO,
	// Month.JUNE, 1))).build());
	// }

	// private void agregarGrupos() {
	// Grupo group1 = new Grupo("Grupo #1", 1);
	// Grupo group2 = new Grupo("Grupo #2", 2);
	// Grupo group3 = new Grupo("Grupo #3", 3);
	// Grupo group4 = new Grupo("Grupo #4", 4);
	// Grupo group5 = new Grupo("Grupo #5", 5);
	// Grupo group6 = new Grupo("Grupo #6", 6);
	// Grupo group7 = new Grupo("Grupo #7", 7);
	// Grupo group8 = new Grupo("Grupo #8", 8);
	// Grupo group9 = new Grupo("Grupo #9", 9);
	//
	// RepositorioGrupo.getInstance().addGroup(group1);
	// RepositorioGrupo.getInstance().addGroup(group2);
	// RepositorioGrupo.getInstance().addGroup(group3);
	// RepositorioGrupo.getInstance().addGroup(group4);
	// RepositorioGrupo.getInstance().addGroup(group5);
	// RepositorioGrupo.getInstance().addGroup(group6);
	// RepositorioGrupo.getInstance().addGroup(group7);
	// RepositorioGrupo.getInstance().addGroup(group8);
	// RepositorioGrupo.getInstance().addGroup(group9);
	// }
	//
	// private void agregarDocentes() {
	// Docente jmc = new Docente("Juan Manuel Cugat", "juanma.cugat@gmail.com",
	// prueba);
	// Docente silvia = new Docente("Silvia Rial", "silvia_rial@yahoo.com.ar",
	// prueba);
	// Docente mariano = new Docente("Mariano Gecik", "mariano.gecik@gmail.com",
	// prueba);
	// Docente nicolas = new Docente("Nicolas Ziella", "nziella@hotmail.com",
	// prueba);
	//
	// RepositorioDocente.getInstance().agregarDocente(jmc);
	// RepositorioDocente.getInstance().agregarDocente(silvia);
	// RepositorioDocente.getInstance().agregarDocente(nicolas);
	// RepositorioDocente.getInstance().agregarDocente(mariano);
	// }

	// private void agregarAlumnos() {
	//
	// Alumno alumno1 = new Alumno("Franco Altamirano", "fra.alt.11@gmail.com",
	// prueba, "145010-4",
	// RepositorioGrupo.getInstance().getGroupByNumber(9));
	// Alumno alumno2 = new Alumno("Nicolas Anderson",
	// "nicolas.anderson@gmail.com", prueba, "138434-0",
	// RepositorioGrupo.getInstance().getGroupByNumber(1));
	// Alumno alumno3 = new Alumno("Roberto Arcangeli",
	// "roberarcangeli@gmail.com", prueba, "151913-0",
	// RepositorioGrupo.getInstance().getGroupByNumber(3));
	// Alumno alumno4 = new Alumno("Gabriel Barlagelata",
	// "gabrielbarbagelata@gmail.com", prueba, "146440-1",
	// RepositorioGrupo.getInstance().getGroupByNumber(6));
	// Alumno alumno5 = new Alumno("Gabriel Bernardini",
	// "gabrielbarbagelata@gmail.com", prueba, "141357-0",
	// RepositorioGrupo.getInstance().getGroupByNumber(9));
	// Alumno alumno6 = new Alumno("Rolando Ezequiel Biondi",
	// "rolando.e.biondi@gmail.com", prueba, "155584-4",
	// RepositorioGrupo.getInstance().getGroupByNumber(4));
	// Alumno alumno7 = new Alumno("David Nicolas Bogo",
	// "davidnicolasbogo@gmail.com", prueba, "149870-8",
	// RepositorioGrupo.getInstance().getGroupByNumber(8));
	// Alumno alumno8 = new Alumno("Miguel Angel Chauca Sanchez",
	// "miguel.sanchez528@gmail.com", prueba, "XXXXXX-X",
	// RepositorioGrupo.getInstance().getGroupByNumber(7));
	// Alumno alumno9 = new Alumno("Federico Constantino",
	// "costantino.fe@gmail.com", prueba, "157343-8",
	// RepositorioGrupo.getInstance().getGroupByNumber(3));
	// Alumno alumno10 = new Alumno("Dario Correa", "dario.correa04@gmail.com",
	// prueba, "131442-7",
	// RepositorioGrupo.getInstance().getGroupByNumber(6));
	//
	// RepositorioAlumno.getInstance().agregarAlumno(alumno1);
	// RepositorioAlumno.getInstance().agregarAlumno(alumno2);
	// RepositorioAlumno.getInstance().agregarAlumno(alumno3);
	// RepositorioAlumno.getInstance().agregarAlumno(alumno4);
	// RepositorioAlumno.getInstance().agregarAlumno(alumno5);
	// RepositorioAlumno.getInstance().agregarAlumno(alumno6);
	// RepositorioAlumno.getInstance().agregarAlumno(alumno7);
	// RepositorioAlumno.getInstance().agregarAlumno(alumno8);
	// RepositorioAlumno.getInstance().agregarAlumno(alumno9);
	// RepositorioAlumno.getInstance().agregarAlumno(alumno10);
	//
	// }

}
