package utn.frba.ads.main;

import static spark.Spark.before;
import static spark.Spark.get;
import static spark.Spark.halt;
import static spark.Spark.post;
import static spark.SparkBase.port;
import static spark.SparkBase.staticFileLocation;

import spark.template.handlebars.HandlebarsTemplateEngine;
import utn.frba.ads.controller.CalendarioController;
import utn.frba.ads.controller.DashboardController;
import utn.frba.ads.controller.SigninController;
import utn.frba.ads.controller.SignupController;
import utn.frba.ads.entities.Usuario;
import utn.frba.ads.utils.AuthenticationUtil;

public class Routes {

	public static void main(String[] args) {

		DashboardController dashboardController = new DashboardController();
		SigninController signinController = new SigninController();
		SignupController signupController = new SignupController();
		CalendarioController calendarController = new CalendarioController();

		HandlebarsTemplateEngine engine = new HandlebarsTemplateEngine();

		port(getHerokuAssignedPort());
		staticFileLocation("/public");

		get("/", signinController::show, engine);
		before("/", (req, res) -> {
			Usuario usuario = AuthenticationUtil.getAuthenticatedUser(req);
			if (usuario != null) {
				res.redirect("/dashboard");
				halt();
			}
		});

		get("/register", signupController::show, engine);
		before("/register", (req, res) -> {
			Usuario user = AuthenticationUtil.getAuthenticatedUser(req);
			if (user != null) {
				res.redirect("/dashboard");
				halt();
			}
		});

		post("/enter", signinController::login, engine);
		post("/register", signupController::register);

		get("/exit", (request, response) -> {
			AuthenticationUtil.removeAuthenticationUser(request);
			response.redirect("/");
			halt();
			return null;
		});

		get("/dashboard", dashboardController::show, engine);
		before("/dashboard", (req, res) -> {
			Usuario user = AuthenticationUtil.getAuthenticatedUser(req);
			if (user == null) {
				res.redirect("/dashboard");
				halt();
			}
		});

		get("/calendario", calendarController::show, engine);
		before("/calendario", (req, res) -> {
			Usuario user = AuthenticationUtil.getAuthenticatedUser(req);
			if (user == null) {
				res.redirect("/dashboard");
				halt();
			}
		});

		// get("/alumnos", studentsController::showStudents, engine);
		// before("/alumnos", (req, res) -> {
		// Usuario user = AuthenticationUtil.getAuthenticatedUser(req);
		// if (user == null) {
		// res.redirect("/dashboard");
		// halt();
		// }
		// });

		// get("/asistencias", asistenciasController::mostrarAsistencias,
		// engine);
		// before("/asistencias", (req, res) -> {
		// Usuario user = AuthenticationUtil.getAuthenticatedUser(req);
		// if (user == null) {
		// res.redirect("/dashboard");
		// halt();
		// }
		// });
		//
		// post("/asistencias/:id", asistenciasController::nuevaAsistencia);
		// before("/asistencias/:id", (req, res) -> {
		// Usuario user = AuthenticationUtil.getAuthenticatedUser(req);
		// if (user == null) {
		// res.redirect("/dashboard");
		// halt();
		// }
		// });

		// post("/alumnos/nuevo", studentsController::addNewStudent, engine);
		// after("/alumnos/nuevo", (req, res) -> {
		// Thread.sleep(1000);
		// res.redirect("/alumnos");
		// });
		//
		// get("/alumnos/:id", studentsController::showStudent, engine);
		// before("/alumnos/:id", (req, res) -> {
		// Usuario user = AuthenticationUtil.getAuthenticatedUser(req);
		// if (user == null) {
		// res.redirect("/dashboard");
		// halt();
		// }
		// });

		// get("/grupos", groupsController::show, engine);
		// before("/grupos", (req, res) -> {
		// Usuario user = AuthenticationUtil.getAuthenticatedUser(req);
		// if (user == null) {
		// res.redirect("/dashboard");
		// halt();
		// }
		// });
		//
		// post("/grupos/nuevo", groupsController::addNewGroup, engine);
		// after("/grupos/nuevo", (req, res) -> {
		// res.redirect("/grupos");
		// });

		// get("/calificaciones", qualificationController::show, engine);
		// before("/calificaciones", (req, res) -> {
		// Usuario user = AuthenticationUtil.getAuthenticatedUser(req);
		// if (user == null) {
		// res.redirect("/dashboard");
		// halt();
		// }
		// });

		// get("/trabajos", worksController::show, engine);
		// before("/trabajos", (req, res) -> {
		// Usuario user = AuthenticationUtil.getAuthenticatedUser(req);
		// if (user == null) {
		// res.redirect("/dashboard");
		// halt();
		// }
		// });
	}

	static int getHerokuAssignedPort() {
		ProcessBuilder processBuilder = new ProcessBuilder();
		if (processBuilder.environment().get("PORT") != null) {
			return Integer.parseInt(processBuilder.environment().get("PORT"));
		}
		return 4567;
	}

}
