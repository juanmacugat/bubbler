Bubbler
=======
A micro java web application for course administration

Build status
------------
[![Build Status](https://travis-ci.org/juanmacugat/bubbler.svg?branch=master)](https://travis-ci.org/juanmacugat/bubbler)

Codecov status
--------------
[![codecov](https://codecov.io/gh/juanmacugat/bubbler/branch/master/graph/badge.svg)](https://codecov.io/gh/juanmacugat/bubbler)
